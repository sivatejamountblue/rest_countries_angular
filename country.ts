export interface Country {
    flags: {
        png: string
    }
    name: {
        common: string,
        official: string
    };
    population: number;
    region: string;
    subregion: string;
    tld: string;
    capital: string;
    borders:[
        string
    ]

  }
  