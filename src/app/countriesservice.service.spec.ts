import { TestBed } from '@angular/core/testing';

import { CountriesserviceService } from './countriesservice.service';

describe('CountriesserviceService', () => {
  let service: CountriesserviceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CountriesserviceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
