import { Component, OnInit } from '@angular/core';
import { CountriesserviceService } from '../countriesservice.service';
import { Country } from 'country';

@Component({
  selector: 'app-countries',
  templateUrl: './countries.component.html',
  styleUrls: ['./countries.component.css']
})
export class CountriesComponent implements OnInit {
  countries: Country [] = [];

  constructor(private service: CountriesserviceService) { }

  ngOnInit(): void {
    this.service.getCountries()
        .subscribe(response => {
          this.countries = response;
        });
  }
  

}
