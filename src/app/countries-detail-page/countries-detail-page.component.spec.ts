import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CountriesDetailPageComponent } from './countries-detail-page.component';

describe('CountriesDetailPageComponent', () => {
  let component: CountriesDetailPageComponent;
  let fixture: ComponentFixture<CountriesDetailPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CountriesDetailPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CountriesDetailPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
