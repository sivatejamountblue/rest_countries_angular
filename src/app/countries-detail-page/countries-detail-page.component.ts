import { Component, OnInit } from '@angular/core';
import { CountriesserviceService } from '../countriesservice.service';
import { ActivatedRoute } from '@angular/router';
import { Country } from 'country';

@Component({
  selector: 'app-countries-detail-page',
  templateUrl: './countries-detail-page.component.html',
  styleUrls: ['./countries-detail-page.component.css']
})
export class CountriesDetailPageComponent implements OnInit {
  countries: Country [] = [];

  constructor(private service: CountriesserviceService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    const name = (this.route.snapshot.paramMap.get('name'))
    console.log(name)
    this.service.getCountries()
        .subscribe(response => {
          this.countries = response.filter((countryname)=>{
            return countryname.name.common === name
          });
        });
  }

}
