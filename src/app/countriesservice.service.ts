import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable} from 'rxjs';
import { Country } from 'country';

@Injectable({
  providedIn: 'root'
})
export class CountriesserviceService {
  private countriesUrl = 'https://restcountries.com/v3.1/all'; 

  constructor(private http: HttpClient) { }
  getCountries(): Observable<Country[]>{
    return this.http.get<Country[]>(this.countriesUrl);
  }
 
}
