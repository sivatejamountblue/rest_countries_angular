import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CountriesComponent } from './countries/countries.component';
import { CountriesDetailPageComponent } from './countries-detail-page/countries-detail-page.component';

const routes: Routes = [
  {
    path: '', component: CountriesComponent
  },
  {
    path: ':name', component: CountriesDetailPageComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [CountriesDetailPageComponent]  

